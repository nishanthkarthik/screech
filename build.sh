#!/usr/bin/env bash

set -e

cd ui
if [[ ! -d node_modules ]]; then
    npm install
fi

npx ng build --prod

cd ..

rm -rf static
mkdir -p static
cp -r ui/dist/screech/* static