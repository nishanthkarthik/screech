from flask import Flask, send_from_directory, redirect, jsonify
from model import Roaster
import random

app = Flask(__name__, static_url_path='')

roaster = Roaster('./model/roasts.txt', './model/speech_model_prefinal.ckpt')
roaster.construct_model()


@app.route('/')
def index():
    return app.send_static_file('index.html')


@app.route('/api/roast')
def roast():
    return jsonify({
    	"text": roaster.sample(100, chr(random.randint(ord('a'), ord('z'))))
    })
    