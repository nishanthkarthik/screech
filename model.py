import json
import re
import os
import tensorflow as tf
import numpy as np

import keras
import keras.layers as L


class Roaster():
    """Generate roasts from model"""

    def __init__(self, data_file, model_file):
        self.model_file = model_file
        self.read_input(data_file)
        self.batch_size = 128
        self.seq_length = 200
        self.vocab_size = len(self.id2c)
        self.data_size = len(self.data)
        self.in_embed_size = 128
        self.hidden_state_size = 768
        self.i_size = self.in_embed_size

    def read_input(self, data_file):
        self.data = open(data_file).read()
        self.id2c = list(set(self.data))
        self.id2c.sort()
        self.c2id = {c: i for i, c in enumerate(self.id2c)}

    def encode_string(self, string_in):
        return np.array([self.c2id[c] for c in string_in])

    def decode_string(self, encoded_str):
        return ''.join([self.id2c[c] for c in encoded_str])

    def construct_model(self):
        self.embed = L.Embedding(self.vocab_size, self.in_embed_size)
        self.lstm_cell = tf.nn.rnn_cell.LSTMCell(self.hidden_state_size)
        self.d_l = L.Dense(self.vocab_size)

        self.inp = tf.placeholder(
            tf.float32, [self.batch_size, self.seq_length])

        self.inp_embed = self.embed(self.inp)
        self.output, _ = tf.nn.dynamic_rnn(
            self.lstm_cell, self.inp_embed, initial_state=self.lstm_cell.zero_state(self.batch_size, tf.float32))

        self.flattened_op = tf.reshape(
            self.output, [-1, self.hidden_state_size])
        self.op_embed = self.d_l(self.flattened_op)

        self.op_embed_shapely = tf.reshape(
            self.op_embed, shape=[self.batch_size, self.seq_length, self.vocab_size])

        self.y = tf.placeholder(tf.int64, [self.batch_size, self.seq_length])
        self.loss = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(
            logits=self.op_embed_shapely, labels=self.y))

        self.h_in = tf.placeholder(tf.float32, [1, self.hidden_state_size])
        self.o_in = tf.placeholder(tf.float32, [1, self.hidden_state_size])
        self.deeznutz = tf.placeholder(tf.int32, shape=[1, 1])

        self.encoded_X = self.embed(inputs=self.deeznutz)
        self.o_out_3d, self.h_out_3d = tf.nn.dynamic_rnn(self.lstm_cell, self.encoded_X,
                                                         initial_state=tf.nn.rnn_cell.LSTMStateTuple(self.h_in, self.o_in))
        self.o_out = tf.reshape(self.o_out_3d, [-1, self.hidden_state_size])
        self.h_out = tf.reshape(self.h_out_3d.c, [-1, self.hidden_state_size])
        self.chars_prob = tf.nn.softmax(self.d_l(self.o_out))
        self.tfchoice = tf.py_func(lambda x, s: np.random.choice(x, p=s[0]), [
                                   self.vocab_size, self.chars_prob], tf.int64, False)

        self.optimizer = tf.train.AdamOptimizer()
        self.step = self.optimizer.minimize(self.loss)

        self.sess = tf.Session()
        self.sess.run(tf.global_variables_initializer())

        tf.train.Saver().restore(self.sess, self.model_file)

    def sample(self, sentence_length, starter):
        h_prev = np.zeros((1, self.hidden_state_size))
        o_prev = np.zeros((1, self.hidden_state_size))

        char_string = [self.c2id[ch] for ch in starter]

        for ch in starter:
            result = self.sess.run([self.tfchoice, self.h_out, self.o_out], {self.deeznutz: [
                              [self.c2id[ch]]], self.h_in: h_prev, self.o_in: o_prev})
            h_prev, o_prev = result[1:]

        for i in range(400):
            result = self.sess.run([self.tfchoice, self.h_out, self.o_out], {self.deeznutz: [
                              [char_string[-1]]], self.h_in: h_prev, self.o_in: o_prev})
            char_string.append(result[0])
            h_prev, o_prev = result[1:]

        return self.decode_string(char_string)
